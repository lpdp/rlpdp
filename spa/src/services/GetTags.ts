import ITag from "../models/Tag"
import Settings from "../config/Settings"

const GetTags = async (): Promise<ITag[]> => {

    const r = await fetch(Settings.rlApiUrl + "/tags")
    if (r.ok === true) {
        return r.json()
    } else {
        const rTags: ITag[] = []
        return rTags
    }
}

export default GetTags