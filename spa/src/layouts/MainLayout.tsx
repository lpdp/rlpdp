import { TApiResponse, useApiGet } from '../hooks/useApiGet'
import { useContext, useEffect, useState } from 'react'

import Select from 'react-select'
import settings from '../config/Settings'

interface Props {
    children: React.ReactNode
}

interface TagOption {
    label: string,
    value: string
}

const MainLayout: React.FunctionComponent<Props> = (props: Props) => {

    const initialTags: TagOption[] = []
    const [tags, setTags] = useState<TagOption[]>(initialTags)
    const tagData: TApiResponse = useApiGet(settings.rlApiUrl + "/api/tags")
    useEffect(() => {
        if (tagData) {
            setTags((tagData.data) as TagOption[]);
        }

    }, [tagData])
    return (
        <div className="app-main-layout">
            <div className="app-toolbar">
                <Select
                    options={tags}
                    isMulti
                    placeholder="Sélectionnez des étiquettes pour filtrer"
                    className='react-select-container'
                    classNamePrefix="react-select" />
            </div>
            <div className="app-content">
                {props.children}
            </div>
        </div>
    )
}

export default MainLayout;