import './App.scss';

import { Link, Outlet } from "react-router-dom";

import HeaderButton from "./components/HeaderButton";
import MyMessages from './components/MyMessages';
import MyProfile from './components/MyProfile';
import MySettings from './components/MySettings';
import SectionsNav from './components/SectionsNav';
import { useGlobalContext } from './GlobalContext';

function App() {
  const { config } = useGlobalContext()

  return (
    <div className="App">
      <div>
        <header className="app-header">
          <h1><Link to="/">La Passion des Poèmes</Link></h1>

          {config.authenticated ? (<div className="app-shortcuts"><MyMessages /><MyProfile /><MySettings /></div>)
            : (
              <div>
                <HeaderButton className='app-btn' link='login'>Connexion</HeaderButton>
                <HeaderButton className='app-btn' link='register'>Inscription</HeaderButton>
              </div>
            )}
        </header>
        <nav>
          <ul>
            <SectionsNav></SectionsNav>
            <li>
              <Link to="members">Membres</Link>
            </li>
            <li>
              <Link to="moderation">Modération</Link>
            </li>
            <li>
              <Link to="admin">Administration</Link>
            </li>
          </ul>
        </nav>
        <Outlet></Outlet>
      </div>
    </div>
  );
}

export default App;
