interface Post {
    id?: number;
    createdAt?: Date;
    modifiedAt?: Date;
    removedAt?: Date;
    title?: string;
    summary?: string;
}

interface LastestPostsProps {
    title: string;
    posts?: Post[]
}

function PostItem(post: Post, i: number) {
    return <div>
        post.title
    </div>
}

export default function LatestPosts(props: LastestPostsProps) {
    const nothingToDisplay = props.posts?.length === 0 || props.posts === undefined;
    if (nothingToDisplay) {
        return (<div>
            <h3>{props.title}</h3>
            <div>Rien à afficher</div>
        </div>)
    } else {
        return (<div>
            <h3>{props.title}</h3>
            {props.posts?.map(PostItem)}
        </div>);
    }
}