import { Link } from "react-router-dom"
import { useGlobalContext } from "../GlobalContext"

const MyProfile = () => {
    const { config } = useGlobalContext()
    return <Link to="myProfile">Mon profil ({config.userName})</Link>
}
export default MyProfile