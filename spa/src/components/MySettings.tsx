import { Link } from "react-router-dom"

const MySettings = () => {
    return <Link to="mySettings">Mes paramètres</Link>
}
export default MySettings