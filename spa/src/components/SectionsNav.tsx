import { TApiResponse, useApiGet } from "../hooks/useApiGet"
import { useEffect, useState } from "react"

import { Link } from "react-router-dom"
import settings from "../config/Settings"

interface SectionNavItem {
    label: string,
    name: string
}



const SectionsNav = () => {
    const initialSectionNavItems: SectionNavItem[] = []
    const [sections, setSection] = useState<SectionNavItem[]>(initialSectionNavItems)
    const sniData: TApiResponse = useApiGet(settings.rlApiUrl + "/api/sections?navbarPinned=true")
    useEffect(() => {
        if (sniData) {
            setSection((sniData.data) as SectionNavItem[]);
        }

    }, [sniData])
    //return <div>{sniData.data}</div>
    console.log(sections)
    return <>
        {sections?.map((e: SectionNavItem, i) => {
            return (<li><Link to={e.label}>{e.name}</Link></li>)
        })}
    </>
}
export default SectionsNav