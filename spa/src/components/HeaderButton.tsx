import { Link } from "react-router-dom"

interface HeaderButtonProps {
    className: string
    link: string
    children: string
}

const HeaderButton = (props: HeaderButtonProps) => {
    return <div className={props.className}><Link to={props.link}>{props.children}</Link></div>
}
export default HeaderButton