import { Link } from "react-router-dom"

const MyMessages = () => {
    const nbMessages = 0
    return <Link to="myMessages">Mes messages ({nbMessages})</Link>
}
export default MyMessages