import './fonts/Lora-Bold.woff2'
import './fonts/Lora-BoldItalic.woff2'
import './fonts/Lora-Italic.woff2'
import './fonts/Lora-Medium.woff2'
import './fonts/Lora-MediumItalic.woff2'
import './fonts/Lora-Regular.woff2'
import './fonts/Lora-SemiBold.woff2'
import './fonts/Lora-SemiBoldItalic.woff2'
import './index.scss';

import {
  RouterProvider,
  createBrowserRouter,
} from "react-router-dom";

import Administration from './pages/Administration'
import Alerts from './pages/Moderation/Alerts'
import App from './App';
import Discussions from './pages/Discussions'
import ErrorPage from './pages/ErrorPage'
import Events from './pages/Events'
import Home from './pages/Home'
import LitteraryGames from './pages/LitteraryGames'
import Login from './pages/Login'
import Members from './pages/Members'
import Moderation from './pages/Moderation'
import MyMessages from './pages/My/MyMessages'
import MyProfile from './pages/My/MyProfile'
import MySettings from './pages/My/MySettings'
import News from './pages/News'
import React from 'react';
import ReactDOM from 'react-dom/client';
import Register from './pages/Register'
import Stats from './pages/Administration/Stats'
import Texts from './pages/Texts'
import Warnings from './pages/Moderation/Warnings'
import reportWebVitals from './reportWebVitals';

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: "",
        element: <Home></Home>
      },
      {
        path: "login",
        element: <Login></Login>
      },
      {
        path: "register",
        element: <Register></Register>
      },
      {
        path: "news",
        element: <News></News>
      },
      {
        path: "texts",
        element: <Texts></Texts>
      },
      {
        path: "events",
        element: <Events></Events>
      },
      {
        path: "litteraryGames",
        element: <LitteraryGames></LitteraryGames>
      },
      {
        path: "discussions",
        element: <Discussions></Discussions>
      },
      {
        path: "members",
        element: <Members></Members>
      },
      {
        path: "moderation",
        element: <Moderation></Moderation>,
        children: [
          {
            path: "alerts",
            element: <Alerts></Alerts>
          },
          {
            path: "warnings",
            element: <Warnings></Warnings>
          }
        ]
      },
      {
        path: "admin",
        element: <Administration></Administration>,
        children: [
          {
            path: "stats",
            element: <Stats></Stats>
          },
          {
            path: "tags",
            element: <Warnings></Warnings>
          }
        ]
      },
      {
        path: "myMessages",
        element: <MyMessages></MyMessages>
      },
      {
        path: "myProfile",
        element: <MyProfile></MyProfile>
      },
      {
        path: "mySettings",
        element: <MySettings></MySettings>
      }
    ]
  },

]);

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
