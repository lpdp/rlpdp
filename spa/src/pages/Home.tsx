import LatestPosts from "../components/LatestPosts";

export default function Home() {
    return <div>
        <LatestPosts title="Dernières Actualités"></LatestPosts>
    </div>;
}