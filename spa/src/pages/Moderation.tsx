import { Link, Outlet } from "react-router-dom";

export default function Moderation() {
    return <div>
        <h2>Modération</h2>
        <Link to="warnings">Avertissements</Link>
        <Link to="alerts">Signalements</Link>
        <Outlet></Outlet>
    </div>
}