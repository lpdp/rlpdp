import React, { useState } from "react";

import { useForm } from "../hooks/useForm";

export default function Login() {
    // defining the initial state for the form
    const initialState = {
        email: "",
        password: "",
    };

    // getting the event handlers from our custom hook
    const { onChange, onSubmit, values } = useForm(
        loginUserCallback,
        initialState
    );

    // a submit function that will execute upon form submission
    async function loginUserCallback() {
        // send "values" to database
    }

    return (
        // don't mind this ugly form :P
        <form className="app-signin-form" onSubmit={onSubmit}>
            <div>
                <input
                    name='email'
                    id='email'
                    type='email'
                    placeholder='Courriel'
                    onChange={onChange}
                    required
                />

                <input
                    name='password'
                    id='password'
                    type='password'
                    placeholder='Mot de passe'
                    onChange={onChange}
                    required
                />
                <input className="app-btn" type="submit" value="Se connecter" />
            </div>
        </form>
    );
}