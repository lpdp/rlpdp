import { Link, Outlet } from "react-router-dom";

export default function Administration() {
    return (<div>
        <h2>Administration</h2>
        <div className="app-console">
            <div className="app-console-side-menu">
                <Link to="stats">Statistiques</Link>
                <Link to="tags">Étiquettes</Link>
                <Link to="removeMember">Effacer un membre</Link>
            </div>
        </div>
        <Outlet></Outlet>
    </div>);
}