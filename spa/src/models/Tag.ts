export interface ITag {
    id: number
    value: string
    label: string
    description: string
    createdAt: number
    startAt: number
    endAt: number
    mature: boolean
}
export default class Tag {
    id: number
    value: string
    label: string
    description: string = ""
    createdAt: number = Math.floor(Date.now() / 1000)
    startAt: number = 0
    endAt: number = 0
    mature: boolean = false

    constructor(id: number, value: string, label: string) {
        this.id = id
        this.value = value
        this.label = label
    }
}