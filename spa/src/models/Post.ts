export default class Post {
    id?: number;
    createdAt?: Date;
    modifiedAt?: Date;
    removedAt?: Date;
    title?: string;
    summary?: string;
    content?: string;

    constructor(id?: number,
        createdAt?: Date,
        modifiedAt?: Date,
        removedAt?: Date,
        title?: string,
        summary?: string,
        content?: string) {
        this.id = id;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
        this.removedAt = removedAt;
        this.title = title;
        this.summary = summary;
        this.content = content;
    }
}