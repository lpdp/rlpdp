import { AccountType } from "../enums/AccountType";

export default class User {
    id?: number;
    username?: string;
    firstname?: string;
    lastname?: string;
    dateOfBirth?: Date;
    avatar?: URL;
    accountType?: AccountType

    constructor(
        id?: number,
        username?: string,
        firstname?: string,
        lastname?: string,
        dateOfBirth?: Date,
        avatar?: URL,
        accountType?: AccountType) {
        this.id = id;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.dateOfBirth = dateOfBirth;
        this.avatar = avatar;
        this.accountType = accountType;
    }
}